#include <vtkSphereSource.h>
#include "vtkConeSource.h"
#include "vtkCylinderSource.h"
#include <vtkProperty.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include "vtkPointData.h"
#include "vtkPoints.h"
#include "vtkCamera.h"
#include "vtkCellArray.h"
#include "vtkFloatArray.h"
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>

int main(int, char *[])
{
  int i;
  //Initial configuration of My Window
  vtkSmartPointer<vtkRenderWindow> renderWindow=
    vtkSmartPointer<vtkRenderWindow>::New();
    renderWindow->SetSize(500, 500);

  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
    vtkSmartPointer<vtkRenderWindowInteractor>::New();

  renderWindowInteractor->SetRenderWindow(renderWindow);

  // Define viewport ranges
  // We set every view by its minimum and maximum x & y  planes
  // in this form > {xPmin,yPmin,xPmax,yPmax}
  double coneView     [4]   = {0.0,0.0,0.5,0.5};
  double sphereView   [4]   = {0.5,0.0,1.0,0.5};
  double cubeView     [4]   = {0.0,0.5,0.5,1.0};
  double cylinderView [4]   = {0.5,0.5,1.0,1.0};

/***************************** Cone Drawer - Bottom Left **********************/
  vtkSmartPointer<vtkRenderer> rendCone =
     vtkSmartPointer<vtkRenderer>::New();
  renderWindow->AddRenderer(rendCone);
  rendCone->SetViewport(coneView);

  // Create a cone
  vtkConeSource *cone = vtkConeSource::New();
  cone->SetHeight( 2.0 );
  cone->SetRadius( 1.0 );
  cone->SetResolution(15);
  cone->Update();

  // Create a mapper and actor
  vtkPolyDataMapper *coneMapper = vtkPolyDataMapper::New();
  coneMapper->SetInputConnection( cone->GetOutputPort() );
  vtkActor *coneActor = vtkActor::New();
  coneActor->SetMapper( coneMapper );
  coneActor->GetProperty()->SetColor(0.052,0.25,0.35);

  // The usual rendering stuff.
  rendCone->AddActor(coneActor);
  rendCone->SetBackground(1.0,1.0, 1.0);
  rendCone->ResetCamera();
/***************************** Cone Drawer - Bottom Left **********************/

/*************************** Sphere Drawer - Bottom Right *********************/

  vtkSmartPointer<vtkRenderer> rendShpere =
      vtkSmartPointer<vtkRenderer>::New();

  renderWindow->AddRenderer(rendShpere);
  rendShpere->SetViewport(sphereView);

  // Create a sphere
  vtkSphereSource *sphere = vtkSphereSource::New();
  sphere->SetCenter(0.0, 0.0, 0.0);
  sphere->SetRadius(5);
  sphere->Update();

  // Create a mapper and actor
  vtkSmartPointer<vtkPolyDataMapper> sphereMapper =
      vtkSmartPointer<vtkPolyDataMapper>::New();
  sphereMapper->SetInputConnection(sphere->GetOutputPort());
  vtkSmartPointer<vtkActor> sphereActor = vtkSmartPointer<vtkActor>::New();
  sphereActor->SetMapper(sphereMapper);
  sphereActor->GetProperty()->SetColor(0.7,0.098,0.098);

  // The usual rendering stuff.
  rendShpere->AddActor(sphereActor);
  rendShpere->SetBackground(0.376,0.623,0.623);
  rendShpere->ResetCamera();
/*************************** Sphere Drawer - Bottom Right *********************/

/***************************** Cube Drawer - Top Left *************************/

  vtkSmartPointer<vtkRenderer> rendCube =
  vtkSmartPointer<vtkRenderer>::New();
  renderWindow->AddRenderer(rendCube);
  rendCube->SetViewport(cubeView);

  // Define cube and its points and polys
  vtkPolyData *cube = vtkPolyData::New();
  vtkPoints *points = vtkPoints::New();
  vtkCellArray *polys = vtkCellArray::New();

  // Create a cube

  //Points defining the cube : 8 points; 3 float values (x,y,z) for each
  //2-D array is suitable to describe this
  static float x[8][3]={{0,0,0}, {0.5,0,0}, {0.5,0.5,0}, {0,0.5,0},
                        {0,0,0.5}, {0.5,0,0.5}, {0.5,0.5,0.5}, {0,0.5,0.5}};

  // We need to render cells and points not only points
  // Cube have 6 cells; 4 points for each
  // these points already defined, so we need to such identification
  // vtkIdType: integer type used for making ID for each cell
  static vtkIdType pts[6][4]={{0,1,2,3}, {4,5,6,7}, {0,1,5,4},
                        {1,2,6,5}, {2,3,7,6}, {3,0,4,7}};

  // Load the points, cells, and data attributes.
  for (i=0; i<8; i++) points->InsertPoint(i,x[i]);
  for (i=0; i<6; i++) polys->InsertNextCell(4,pts[i]);

  // We now assign the pieces to the vtkPolyData.
  cube->SetPoints(points);
  points->Delete();
  cube->SetPolys(polys);
  polys->Delete();

  // Create a mapper and actor
  vtkPolyDataMapper *cubeMapper = vtkPolyDataMapper::New();
  cubeMapper->SetInputData(cube);
  cubeMapper->SetScalarRange(0,7);
  vtkActor *cubeActor = vtkActor::New();
  cubeActor->SetMapper(cubeMapper);
  cubeActor->GetProperty()->SetColor(0.376,0.623,0.623);

  // The usual rendering stuff.
  vtkCamera *camera = vtkCamera::New();
  camera->SetPosition(1,1,1);
  camera->SetFocalPoint(0,0,0);
  rendCube->AddActor(cubeActor);
  rendCube->SetActiveCamera(camera);
  rendCube->ResetCamera();
  rendCube->SetBackground(0.7,0.098,0.098);
/***************************** Cube Drawer - Top Left *************************/

/*************************** Cylinder Drawer - Top Right **********************/

  vtkSmartPointer<vtkRenderer> rendCylinder =
  vtkSmartPointer<vtkRenderer>::New();
  renderWindow->AddRenderer(rendCylinder);
  rendCylinder->SetViewport(cylinderView);

  // Create a cylinder
  vtkCylinderSource *cylinder = vtkCylinderSource::New();
  cylinder->SetResolution(8);

  // Create a mapper and actor
  vtkPolyDataMapper *cylinderMapper = vtkPolyDataMapper::New();
  cylinderMapper->SetInputConnection(cylinder->GetOutputPort());
  vtkActor *cylinderActor = vtkActor::New();
  cylinderActor->SetMapper(cylinderMapper);
  cylinderActor->GetProperty()->SetColor(1.0,1.0, 1.1);
  cylinderActor->RotateX(30.0);
  cylinderActor->RotateY(-45.0);

  // The usual rendering stuff.
  rendCylinder->AddActor(cylinderActor);
  rendCylinder->SetBackground(0.052,0.25,0.35);

/*************************** Cylinder Drawer - Top Right **********************/

  renderWindowInteractor->Start();
  return EXIT_SUCCESS;
}
